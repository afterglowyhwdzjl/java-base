# Switch语句

## 1、分支语句switch语句

​     **1）格式：**

```java
switch(表达式){
        case 1：
            语句体1；
            break；//相当于一堵墙。
        case 2：
            语句体2；
            break；
            ......
        default:
            语句体n+1；
            break； 
}
```

​     **2）执行流程：**

​            首先计算出表达式的值

​            其次，和case依次比较，一旦有对应的值，就会执行相应的语句，在执行的过程中，遇到break就会结 束。

​            最后，如果所有的case都和表达式的值不匹配，就会执行default语句体部分，然后程序结束掉。

## 2、 **switch**案例

​      **1）需求：键盘录入星期数，显示今天的减肥活动**：

```
周一：跑步
周二：游泳
周三：慢走
周四：动感单车
周五：拳击
周六：爬山
周日：好好吃一顿
```

​      <b> 2）示例代码：</b>

```java
public static void main(String[] args){
         // 1. 键盘录入星期数据，使用变量接收
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入");
        int week = sc.nextInt();
         // 2. 多情况判断，采用switch语句实现
        switch(week){
         // 3. 在不同的case中，输出对应的减肥计划
        case 1:
           System.out.println("跑步");
           break;
        case 2:
           System.out.println("游泳");
           break;
        case 3:
           System.out.println("慢走");
           break;
        case 4:
           System.out.println("动感单车");
           break;
        case 5:
           System.out.println("拳击");
           break;
        case 6:
           System.out.println("爬山");
           break;
        case 7:
           System.out.println("好好吃一顿");
           break;
        default:
           System.out.println("您的输入有误");
           break;
                   }
          }
}
```

## 3、switch语句case穿透

​            **1）概述 :** 如果switch语句中,case省略了break语句, 就会开始case穿透

​            **2）需求 :** 键盘录入星期数，输出工作日、休息日 (1-5)工作日，(6-7)休息日

​            **3）示例代码：**

```java
/*
case穿透是如何产生的?
    如果switch语句中,case省略了break语句, 就会开始case穿透.
现象：
当开始case穿透，后续的case就不会具有匹配效果，内部的语句都会执行
直到看见break，或者将整体switch语句执行完毕，才会结束。
*/
public static void main(String[] args){
             Scanner sc = new Scanner(System.in);
             System.out.println("请输入星期数:");
             int week = sc.nextInt();
             switch(week){
             case 1:
             case 2:
             case 3:
             case 4:
             case 5:
                 System.out.println("工作日");
                 break;
             case 6:
             case 7:
                 System.out.println("休息日");
                 break;
             default:
                 System.out.println("您的输入有误");
                 break;
            }
      }
}
```



# for循环

## 1、循环语句-for循环

​       <b>1)循环：</b>

​           循环语句可以在满足循环条件的情况下，反复执行某一段代码，这段被重复执行的代码被称为循环体语句，当反复 执行这个循环体时，需要在合适的时候把循环判断条件修改为false，从而结束循环，否则循环将一直执行下去，形 成死循环。

​       <b>2)循环格式：</b>

```java
for(初始化语句;条件判断语句;条件控制语句) {
    循环体语句;
}
```

​      <b> 3)格式解释：</b>

①*初始化语句：* 用于表示循环开启时的起始状态，简单说就是循环开始的时候什么样

②*条件判断语句：*用于表示循环反复执行的条件，简单说就是判断循环是否能一直执行下去

③*循环体语句：* 用于表示循环反复执行的内容，简单说就是循环反复执行的事情

④*条件控制语句：*用于表示循环执行中每次变化的内容，简单说就是控制循环是否能执行下去

​     <b>4)执行流程：</b>

①执行初始化语句

②执行条件判断语句，看其结果是true还是false

​                     如果是false，循环结束

​                     如果是true，继续执行

③执行循环体语句

④执行条件控制语句

⑤回到②继续

## 2、for循环案例-输出数据1-5和5-1

​         <b>1）需求：</b>在控制台输出1-5和5-1的数据

​         **2）示例代码：**

```java
public class ForTest01 {
    public static void main(String[] args) {
         //需求：输出数据1-5
         for(int i=1; i<=5; i++) {
              System.out.println(i);
          }
          System.out.println("--------");
          //需求：输出数据5-1
          for(int i=5; i>=1; i--) {
              System.out.println(i);
        }
    }
}
```

## 3、for循环案例-求1-5数据和

​           <b>1）需求：</b>求1-5之间的数据和，并把求和结果在控制台输出

​           **2）示例代码：**

```java
public class ForTest02 {
public static void main(String[] args) {
//求和的最终结果必须保存起来，需要定义一个变量，用于保存求和的结果，初始值为0
int sum = 0;
//从1开始到5结束的数据，使用循环结构完成
for(int i=1; i<=5; i++) {
//将反复进行的事情写入循环结构内部
// 此处反复进行的事情是将数据 i 加到用于保存最终求和的变量 sum 中
sum += i;
/*
sum += i; sum = sum + i;
第一次：sum = sum + i = 0 + 1 = 1;
第二次：sum = sum + i = 1 + 2 = 3;
第三次：sum = sum + i = 3 + 3 = 6;
第四次：sum = sum + i = 6 + 4 = 10;
第五次：sum = sum + i = 10 + 5 = 15;
*/
}
//当循环执行完毕时，将最终数据打印出来
System.out.println("1-5之间的数据和是：" + sum);
}
}
```

## 4、for循环案例-求1-100偶数和

​          **1）需求：**求1-100之间的偶数和，并把求和结果在控制台输出 

​          **2）示例代码：**

```Java
public class ForTest03{
    public static void main(String[] args){
        //求和的最终结果必须保存起来，需要定义一个变量，用于保存求和的结果，初始值为0
        int sum = 0;
        //对1-100的数据求和与1-5的数据求和几乎完全一样，仅仅是条件不同
        for(int i = 1;i<=100;i++){
            //对1-100的偶数求和，需要对求和操作添加限制条件，判断是否是偶数
            if(i%2==0){
                sum+=i;
            }
        }
        //当循环执行完毕时，将最终数据打印出来
        System.out.println("1-100之间的偶数和是："+sum)
    }
}
```

## 5、for循环案例-水仙花数

​             **1）需求：**在控制台输出所有的“水仙花数”

​             **2）解释：**什么是水仙花数？

水仙花数，指的是一个三位数，个位、十位、百位的数字立方和等于原数

e.g:例如 153 3*3*3 + 5*5*5 + 1*1*1 = 153

思路：

1. 获取所有的三位数，准备进行筛选，最小的三位数为100，最大的三位数为999，使用for循环获取

2. 获取每一个三位数的个位，十位，百位，做if语句判断是否是水仙花数

​             **3）示例代码：**

```Java
public class ForTest04 {
    public static void main(String[] args) {
         //输出所有的水仙花数必然要使用到循环，遍历所有的三位数，三位数从100开始，到999结束
         for(int i=100; i<1000; i++) {
             //在计算之前获取三位数中每个位上的值
             int ge = i%10;
             int shi = i/10%10;
             int bai = i/10/10%10;
             //判定条件是将三位数中的每个数值取出来，计算立方和后与原始数字比较是否相等
             if(ge*ge*ge + shi*shi*shi + bai*bai*bai == i) {
                //输出满足条件的数字就是水仙花数
                System.out.println(i);
              }
         }
    }
}
```

## 6、for循环案例-每行打印2个水仙花数（统计）

​           **1）需求：**在控制台输出所有的“水仙花数”，要求每行打印2个

​           **2）示例代码：**

```java
public class Demo6For {
    /*
        需求：在控制台输出所有的“水仙花数”，要求每行打印2个
        System.out.print (打印内容); 打印后不换行
        System.out.println(打印内容); 打印后换行
        分析:
             1. 定义变量count，用于保存“打印过”的数量，初始值为0
             2. 在判定和打印水仙花数的过程中，拼接空格, 但不换行，并在打  印后让count变量+1，记录打印过的数量
             3. 在每一次count变量+1后，判断是否到达了2的倍数，是的话，换行。
     */
     public static void main(String[] args){
         // 1. 定义变量count，用于保存“打印过”的数量，初始值为0
         int count = 0;
         for(int i = 100; i <= 999; i++){
             int ge = i % 10;
             int shi = i / 10 % 10;
             int bai = i / 10 / 10 % 10;
             
             if( (ge*ge*ge + shi*shi*shi + bai*bai*bai) == i){
                  // 2. 在判定和打印水仙花数的过程中，拼接空格, 但不换行，并在打印后让count变量+1，记录打印过的数量
                  System.out.print(i + " ");
                  count++;
                  // 3. 在每一次count变量+1后，判断是否到达了2的倍数，是的话，换行
                  if(count % 2 == 0){
                        System.out.println();
                  }
              }
         }
    }
}
```

#### 课后练习：

```Java
public class Z1 {
    public static void main(String[] args) {
        //求出100-1000以内的水仙花数，并且每两个数值换行一次。
       int count=0;
        for (int num = 100;num<1000;num++){
            int a = num/100;
            int b = num/10%10;
            int c = num%10;
            if (a*a*a+b*b*b+c*c*c==num){
                System.out.print(num+" ");
                count++;
                if (count%2==0){
                    System.out.println();
                }
            }
        }
    }
}
```

#### Page86练习5：

```java
import java.util.Scanner;

public class Z2 {
    public static void main(String[] args) {
        //某市不同车牌的出租车3千米的起步价和计费分别为：
        //夏利3元，3千米以外，2.1元/千米;
        //富康4元，3千米以外，2.4元/千米;
        //桑塔纳5元，3千米以外，2.7元/千米。
        //编程实现从键盘输入乘车的车型及行车千米数，输出应付车费。(使用switch和if完成)
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入车型：");
        String type = sc.next();
        System.out.println("请输入行车千米数：");
        int num = sc.nextInt();
        double a = (num - 3) * 2.1 + 3;//夏利
        double b = (num - 3) * 2.4 + 4;//富康
        double c = (num - 3) * 2.7 + 5;//桑塔纳
        switch (type) {
            case "夏利":
            case "富康":
            case "桑塔纳":
                //夏利计费：
                if (num < 3 && type.equals("夏利")) {
                    System.out.println("共收费：" + 3 + "元。");
                } else if (num >= 3 && type.equals("夏利")) {
                    System.out.println("共收费：" + a + "元。");
                } //富康计费：
                else if (num < 3 && type.equals("富康")) {
                    System.out.println("共收费：" + 4 + "元。");
                }else if (num >= 3 && type.equals("富康")) {
                    System.out.println("共收费：" + b + "元。");
                } //桑塔纳计费：
                else if (num < 3 && type.equals("桑塔纳")) {
                    System.out.println("共收费：" + 5 + "元。");
                } else if (num >= 3 && type.equals("桑塔纳")) {
                    System.out.println("共收费：" + c + "元。");
                }
                else{
                    System.out.println("请正确输入！");
                }
        }
    }
}
```

#### 拓展作业1：

```Java
import java.util.Scanner;

public class Z3 {
    public static void main(String[] args) {
        //使用switch和if语句算出今天是今年的第几天。
        // （提示：以3月3日为例，应该先把前两个月的天数加起来，然后再加上3天即为本年的第几天：
        // 需要考虑闰年的情况，如果输入的年份是闰年且输入的月份大于或等于3，需要多加1天）
        //平年2月份28天，闰年2月份29天
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入年份：");
        int year = sc.nextInt();
        System.out.println("请输入月份：");
        int month = sc.nextInt();
        System.out.println("请输入日数：");
        int day = sc.nextInt();
        int sum = 0;
        switch (month) {
            case 12:
                sum += 30;
            case 11:
                sum += 31;
            case 10:
                sum += 30;
            case 9:
                sum += 31;
            case 8:
                sum += 31;
            case 7:
                sum += 30;
            case 6:
                sum += 31;
            case 5:
                sum += 30;
            case 4:
                sum += 31;
            case 3:
                sum += 28;
            case 2:
                sum += 31;
            case 1:
                sum += day;
        }
        if (month>=3){
            if (year%400==0||year%4==0&&year%100!=0){
                sum++;
            }
            System.out.println("今天是今年的第："+sum+"天");
        }else if (month<3){
            System.out.println("今天是今年的第："+sum+"天");
        }
    }
}
```



```java
import java.util.Scanner;

public class Z4 {
    public static void main(String[] args) {
        //编写一个程序，根据用户输入的一个字符，判断该字符是不是字母，
        //如果是字母，判断该字母是声母还是韵母，是大写字母还是小写字母；
        //如果不是字母，则输出"你输入的字符不是字母。"
        String[] shm = {"b", "p", "m", "f", "d", "t", "n", "l", "g", "k", "h",
                        "j", "q", "x", "zh", "ch", "sh", "r", "z", "c", "s", "y", "w"};
        String[] dsm = {"B", "P", "M", "F", "D", "T", "N", "L", "G", "K",
                        "H", "J", "Q", "X", "ZH", "CH", "SH", "R", "Z", "C", "S", "Y",
                        "W"};
        //韵母有：a,o,e,i,u,v,ai,ei,ui,ie,ve,er,an,en,in,un,vn,ang,eng,ing,ong.
        String[] xy = {"a", " o", "e", " i", "u", "v"};
        String[] dy = {"A", " O", "E", "I", "U", "V"};
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入一个字符:");
        String letter = sc.next();
                boolean method=false;
                for (int i = 0; i < shm.length; i++) {
                    if (letter.equals(shm[i])) {
                        System.out.println("小写声母：" + letter);
                        method=true;
                        break;
                    }
                }
                for (int i = 0; i < dsm.length; i++) {
                    if (letter.equals(dsm[i])) {
                        System.out.println("大写声母：" + letter);
                        method=true;
                        break;
                    }
                }
                for (int i = 0; i < xy.length; i++) {
                    if (letter.equals(xy[i])) {
                        System.out.println("小写韵母：" + letter);
                        method=true;
                        break;
                    }
                }
                for (int i = 0; i < dy.length; i++) {
                    if (letter.equals(dy[i])) {
                        System.out.println("大写韵母：" + letter);
                        method=true;
                        break;
                    }
                }
                if (!method){
                    System.out.println("你输入的不是字母");
                }
            }
        }
```

